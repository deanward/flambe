<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiResource('menus', 'MenuController');
Route::get('/menus/{menu}/sections','MenuController@sections');
Route::put('/menus/{menu}/sections/{section}','MenuController@addSection');

Route::apiResource('dishes', 'DishController');
Route::get('dishes/{dish}/optiongroups','DishOptionGroupController@index');
Route::get('dishes/{dish}/optiongroups/{group}','DishOptionGroupController@show');
Route::post('/dishes/{dish}/optiongroups','DishController@store');
Route::put('/dishes/{dish}/optiongroups/{group}','DishOptionGroupController@update');
Route::delete('dishes/{dish}/optiongroups/{group}','DishOptionGroupController@destroy');

Route::apiResource('sections','SectionController');
Route::get('/sections/{section}/dishes','SectionController@dishes');
Route::put('/sections/{section}/dishes/{dish}','SectionController@addDish');

