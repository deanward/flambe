<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dishes', function (Blueprint $table) {
            $table->id();
            $table->text('name')->comment('Short name for the dish.');
            $table->text('description')->nullable()->comment('Longer description of the dish.');
            $table->boolean('vegetarian')->default(false)->comment('Indicates if the dish is suitable for vegatarians.');
            $table->boolean('vegan')->default(false)->comment('Indicates if the dish is suitable for vegans.');
            $table->tinyInteger('spice_level')->default(0)->comment('Spice rating of the dish between 0 and 5.');
            $table->decimal('price',6,2)->comment('Base cost of the dish before options are selected.');
            $table->string('image')->nullable()->comment('File name on disk of the image that represents the dish.');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dishes');
    }
}
