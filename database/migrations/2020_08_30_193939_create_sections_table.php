<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sections', function (Blueprint $table) {
            $table->id();
            $table->text('name')->comment('Name of the section.');
            $table->text('description')->nullable()->comment('Longer description for the section.');
            $table->boolean('visible')->default(true)->comment('Should section be visible on the menu.');
            $table->integer('order')->default(0)->comment('Controls the order sections are rendered.');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sections');
    }
}
