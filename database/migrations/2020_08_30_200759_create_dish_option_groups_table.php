<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDishOptionGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dish_option_groups', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('dish_id');
            $table->text('name')->comment('Name of the option group.');
            $table->boolean('single')->comment('When true only one option in group may be selected');
            $table->bigInteger('visible_when')->nullable()->comment('Id of dish option that enables group.');
            $table->decimal('default_group_price',6,2)->nullable()->comment('Default price for items in group.');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dish_option_groups');
    }
}
