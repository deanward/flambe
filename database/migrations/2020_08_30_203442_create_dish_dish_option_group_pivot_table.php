<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDishDishOptionGroupPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dish_dish_option_group', function (Blueprint $table) {
            $table->bigInteger('dish_id')->unsigned()->index();
            $table->foreign('dish_id')->references('id')->on('dish_option_groups')->onDelete('cascade');
            $table->bigInteger('dish_option_group_id')->unsigned()->index();
            $table->foreign('dish_option_group_id')->references('id')->on('dishes')->onDelete('cascade');
            $table->primary(['dish_id', 'dish_option_group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dish_dish_option_group');
    }
}
