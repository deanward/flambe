<?php

namespace App;

use App\Section;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = ['name','description'];
    public $requestable = ['sections.dishes.optionGroups'];

    public function sections() {
        return $this->belongsToMany(Section::class);
    }
}
