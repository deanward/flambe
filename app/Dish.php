<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    protected $fillable = ['name','description','vegetarian','vegan','spice_level','price'];
    public $requestable = ['optionGroups.options'];

    public function optionGroups() {
        return $this->hasMany(DishOptionGroup::class);
    }
}
