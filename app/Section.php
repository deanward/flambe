<?php

namespace App;

use App\Dish;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = ['name','description','visible','order'];

    public function dishes() {
        return $this->belongsToMany(Dish::class);
    }
}
