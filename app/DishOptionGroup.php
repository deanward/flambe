<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DishOptionGroup extends Model
{
    protected $fillable = ['name','single','default_group_price','enabled_by'];
    public $requestable = ['options'];

    public function options() {
        return $this->hasMany(DishOption::class,'group_id');
    }
}
