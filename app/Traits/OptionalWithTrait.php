<?php namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

Trait OptionalWithTrait {
    protected function optionalWith(String $classname, Int $id = null) {
        $classname = 'App\\' . $classname;
        $model = new $classname;
        $whitelist = $model->requestable;
        $whitelist = $this->expandWhitelist($whitelist);
        if($id) {
            $query = $model->where('id',$id);
        } else {
            $query = $model->query();
        }
        foreach(explode(',',request()->with) as $with) {
            if( in_array_r($with,$whitelist) ){
                $query->with($with);
            }
        }
        return $query;
    }

    //expands dot notation items in whitelist into a multi-dimensional array
    //allowing in_array_r() to search for them recursively
    private function expandWhitelist($whitelist) {
        $result = [];
        foreach($whitelist as $item) {
            $result[] = explode('.',$item);
            $result[] = $item;
        }

        return $result;
    }
}
