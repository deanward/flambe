<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;
use App\Traits\OptionalWithTrait;
use App\Http\Resources\MenuResource;
use App\Http\Resources\MenuCollection;
use App\Http\Resources\SectionCollection;

class MenuController extends Controller
{

    use OptionalWithTrait;
    protected $whitelist = ['sections'];

    /**
     * Display a listing of the Menu
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = $this->optionalWith('Menu')->paginate();
        return new MenuCollection($menus);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);

        $menu = new Menu($request->only([
            'name',
            'description'
        ]));

        $menu->save();

        return new MenuResource($menu);
    }

    /**
     * Display the specified Menu
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu = Menu::find($id);

        if($menu) {
            return new MenuResource($menu);
        }

        abort(404);
    }

    /**
     * Display the sections for the specified Menu
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sections($menu)
    {
        $menu = Menu::find($menu);
        if($menu) {
            if($menu->sections) {
                return new SectionCollection($menu->sections()->paginate());
            }
            return null;
        }

        abort(404);
    }


    /**
     * Attach section to the specified Menu
     *
     * @param  mixed  $menu
     * @param  mixed  $section
     * @return \Illuminate\Http\Response
     */
    public function addSection($menu,$section)
    {
        $menu = Menu::find($menu);
        if($menu && $section) {
            $menu->sections()->attach($section);
            return;
        }

        abort(404);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $menu = Menu::find($id);

        if(! $menu ) {
            abort(404);
        }

        $menu->update($request->only([
            'name',
            'description'
        ]));

        return new MenuResource($menu);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::find($id);
        if(! $menu ) {
            abort(404);
        }

        $menu->delete();

        return;
    }
}
