<?php

namespace App\Http\Controllers;

use App\Dish;
use App\DishOptionGroup;
use Illuminate\Http\Request;
use App\Traits\OptionalWithTrait;
use App\Http\Resources\DishResource;
use App\Http\Resources\DishOptionGroupResource;
use App\Http\Resources\DishOptionGroupColletion;
use App\Http\Resources\DishOptionResource;

class DishOptionGroupController extends Controller
{

    use OptionalWithTrait;

    const GROUP_MISSMATCH_ERROR = 'That Option Group does not belong to the Dish specified.';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Dish $dish)
    {
        return new DishOptionGroupColletion($dish->optionGroups);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Dish $dish)
    {
        $request->validate([
            'name' => 'required',
            'single' => 'required|boolean'
        ]);

        $optionGroup = new DishOptionGroup($request->only([
            'name',
            'single',
            'default_group_price'
        ]));

        $dish->optionGroups()->save($optionGroup);

        return new DishResource($dish->with('optionGroups')->first());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DishOptionGroup  $dishOptionGroup
     * @return \Illuminate\Http\Response
     */
    public function show(Dish $dish, DishOptionGroup $group)
    {
        if($dish->id == $group->dish_id) {
            $group = $this->optionalWith("DishOptionGroup",$group->id)->first();
            return new DishOptionGroupResource($group);

        } else {
            return response()->json(['success' => false,'message' => DishOptionGroupController::GROUP_MISSMATCH_ERROR],404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DishOptionGroup  $dishOptionGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dish $dish, DishOptionGroup $group)
    {

        if($dish->id !== $group->dish_id) {
            return response()->json(['success' => false,'message' => DishOptionGroupController::GROUP_MISSMATCH_ERROR],400);
        }

        $request->validate([
            'single' => 'boolean'
        ]);

        $group->update($request->only([
            'name',
            'single',
            'default_group_price'
        ]));

        return new DishOptionGroupResource($group);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DishOptionGroup  $dishOptionGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dish $dish,DishOptionGroup $group)
    {
       if($dish->id == $group->dish_id) {
            $group->delete();
            return;
        } else {
            return response()->json(['success' => false,'message' => DishOptionGroupController::GROUP_MISSMATCH_ERROR],400);
        }
    }
}
