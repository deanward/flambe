<?php

namespace App\Http\Controllers;

use App\Dish;
use App\DishOption;
use App\DishOptionGroup;
use Illuminate\Http\Request;
use App\Traits\OptionalWithTrait;
use App\Http\Resources\DishResource;
use App\Http\Resources\DishCollection;
use App\Http\Resources\DishOptionGroupResource;
use App\Http\Resources\DishOptionGroupColletion;

class DishController extends Controller
{

    use OptionalWithTrait;
    protected $whitelist = ['optionGroups'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dishes = $this->optionalWith('Dish')->paginate();
        return new DishCollection($dishes);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'spice_level' => 'between:0,5'
        ]);

        $dish = new Dish($request->only([
            'name',
            'description',
            'vegetarian',
            'vegan',
            'spice_level',
            'price'
        ]));

        $dish->save();

        return new DishResource($dish);
    }

    /**
     * Display the specified dish.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $dish = $this->optionalWith("Dish",$id)->first();

        if($dish) {
            return new DishResource($dish);
        }

        abort(404);
    }

    /**
     * Update the specified dish in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $dish
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dish $dish)
    {

        $request->validate([
            'spice_level' => 'numeric|between:0,5'
        ]);

        $dish->update($request->only([
            'name',
            'description',
            'vegetarian',
            'vegan',
            'spice_level',
            'price'
        ]));

        return new DishResource($dish);
    }

    /**
     * Remove the specified dish from storage.
     *
     * @param  int  $dish
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dish $dish)
    {
        $dish->delete();
        return;
    }
}
