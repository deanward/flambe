<?php

namespace App\Http\Controllers;

use App\Dish;
use App\Section;
use Illuminate\Http\Request;
use App\Http\Resources\DishCollection;
use App\Http\Resources\SectionResource;
use App\Http\Resources\SectionCollection;

class SectionController extends Controller
{
    /**
     * Display a listing of the section.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections = Section::paginate();
        return new SectionCollection($sections);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'visible' => 'boolean',
            'order' => 'numeric'
        ]);

        $secion = new Section($request->only([
            'name',
            'description',
            'visible',
            'order'
        ]));

        $secion->save();

        return new SectionResource($secion);
    }

    /**
     * Display the specified section.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $section = Section::find($id);
        if($section) {
            return new SectionResource($section);
        }

        abort(404);
    }
    /**
     * Display the dishes in specified section.
     *
     * @param  mixed  $section
     * @return \Illuminate\Http\Response
     */
    public function dishes(Section $section) {
        return new DishCollection($section->dishes()->paginate());
    }

    /**
     * Attach dish to the specified Section
     *
     * @param  mixed  $section
     * @param  mixed  $dish
     * @return \Illuminate\Http\Response
     */
    public function addDish(Section $section, Dish $dish)
    {

        $section->dishes()->attach($dish);
        return;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $section = Section::find($id);

        if(! $section ) {
            abort(404);
        }

         $request->validate([
            'visible' => 'boolean',
            'order' => 'numeric'
        ]);

        $section->update($request->only([
            'name',
            'description',
            'visible',
            'order'
        ]));

        return new SectionResource($section);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section = Section::find($id);

        if(! $section ) {
            abort(404);
        }

        $section->delete();

        return;
    }
}
