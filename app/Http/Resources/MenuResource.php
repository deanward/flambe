<?php

namespace App\Http\Resources;

use App\Http\Resources\SectionCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class MenuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'sections' => $this->when($this->relationLoaded('sections'), function () {
              return new SectionCollection($this->sections);
            }),
            'created' => $this->created_at,
            'last_updated' => $this->updated_at,
            '_self' => url('/api/menus/' . $this->id)
        ];
    }
}
