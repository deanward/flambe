<?php

namespace App\Http\Resources;

use App\Http\Resources\DishOptionGroupColletion;
use Illuminate\Http\Resources\Json\JsonResource;

class DishResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $array = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'vegetarian' => $this->vegetarian,
            'vegan' => $this->vegan,
            'spice_level' => $this->spice_level,
            'price' => $this->price,
            'image' => url('/images/dishes/' . $this->id . '/' . urlencode($this->name) ),
            'optionGroups' => $this->when($this->relationLoaded('optionGroups'), function () {
              return new DishOptionGroupColletion($this->optionGroups);
            }),
            '_self' => url('/api/dishes/' . $this->id),
        ];

        return $array;

    }
}
