<?php

namespace App\Http\Resources;

use App\Http\Resources\DishOptionResource;
use App\Http\Resources\DishOptionGroupResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class DishOptionGroupColletion extends ResourceCollection
{
   public $collects = DishOptionGroupResource::class;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
