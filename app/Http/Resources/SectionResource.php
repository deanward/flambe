<?php

namespace App\Http\Resources;

use App\Http\Resources\DishCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class SectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $array = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'visible' => $this->visible,
            'order' => $this->order,
            'dishes' => $this->when($this->relationLoaded('dishes'), function () {
              return new DishCollection($this->dishes);
            }),
            '_self' => url('/api/sections/' . $this->id)
        ];

        if($request->expand == 'true') {
            $array['dishes'] = new DishCollection($this->dishes);
        }

        return $array;
    }
}
