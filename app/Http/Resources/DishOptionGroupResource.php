<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DishOptionGroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'single' => $this->single,
            'default_group_price' => $this->default_group_price,
            'options' => $this->when($this->relationLoaded('options'), function () {
              return new DishOptionCollection($this->options);
            }),
        ];
    }
}
